# Biology Vocabulary App #

This is an Angular Js app using Gulp for the build.

### Play, Practice, Test your Biology Vocabulary ###

* This was created for Biology students to use to build their vocabulary skills
* Version 1.0
* [Demo this app](http://sprystack.com/biology/app/)

### Still left to do: ###

* Winner Notification
* Start over screen
* Media Queries/Mobile Optimization
* Pass or Fail based on percentage rather than set number
* Deployment instructions
