var gulp = require('gulp'),
    sass = require('gulp-sass'),
    importCss = require('gulp-import-css'),
    concat = require('gulp-concat'),
    del = require('del'),
    uglify = require('gulp-uglify'),
    rename = require('gulp-rename'),
    runSequence = require('run-sequence'),
    connect = require('gulp-connect-php'),
    browserSync = require('browser-sync'),
    spa = require("browser-sync-spa");

var paths = {
  root: '/app',
  scss: './app/assets/scss/',
  //js: './assets/js/',
  distcss: './app/dist/css/',
  distjs: './app/dist/js/'
};

browserSync.use(spa({

    // Only needed for angular apps
    selector: "[ng-app]",

    history: {
        index: '/index.html'
    }
}));

browserSync({
    open: true,
    server: "app/",
    files:  "app/*"
});

// Clean
gulp.task('clean-css', function(cb) {
    del( paths.distcss, cb);
});

// Build CSS
gulp.task('buildcss', function(cb) {
    runSequence(
        ['clean-css',
        'sass'],
        cb
    );
});

gulp.task('sass', function() {
    return gulp.src(paths.scss+'main.scss')
      .pipe(sass())
      .pipe(sass().on('error', sass.logError))
      .pipe(importCss())
      .pipe(gulp.dest(paths.distcss))
      .pipe(browserSync.stream());
});


// gulp.task('serve', function() {
//   connect.server({}, function (){
//     browserSync({
//       proxy: '127.0.0.1:8000'
//     });
//   });
// });

gulp.watch(paths.scss+'*.scss', ['buildcss']);
//gulp.watch(paths.js + "*.js", ['buildjs']);
gulp.watch(['**/*.html', '**/*.js']).on('change', function () {
  browserSync.reload();
});

// // Default task
 gulp.task('default', function(cb) {
    runSequence(
         'buildcss',
        //['serve'],
        cb
    );
 });