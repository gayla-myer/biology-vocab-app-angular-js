(function() {
  var cards = document.querySelectorAll(".card.effect-click");
  for ( var i  = 0, len = cards.length; i < len; i++ ) {
    var card = cards[i];
    clickListener( card );
  }

  function clickListener(card) {
    card.addEventListener( "click", function() {
      var c = this.classList;
      c.contains("flipped") === true ? c.remove("flipped") : c.add("flipped");
    });
  }
})();

jQuery(document).ready(function($) {
  // var bomb = $('.Bomb');
  // bomb.click(function(){

  // });
  var safe = $('.card:not(.Bomb)');
  var kaboom = $('.card.Bomb');
  var count = 0;
  safe.click(function () {
    count += 1;
    if (count == 5) {
      $('.win-result .text').text('Hooray! You Won!');
      setTimeout(function() {
        $('.win-result .text').show('slow');
      }, 300);
      sWin.playclip();

    }
    sSafe.playclip();
      // safe.trigger('cssClassChanged')
      // $(otherSelector).bind('cssClassChanged', data, function(){ do stuff });
  });
  kaboom.click(function () {
    sKaboom.playclip();
  });



// Click sound effect
var html5_audiotypes={ //define list of audio file extensions and their associated audio types. Add to it if your specified audio file isn't on this list:
  "mp3": "audio/mpeg",
  "mp4": "audio/mp4",
  "ogg": "audio/ogg",
  "wav": "audio/wav"
}

function createsoundbite(sound){
  var html5audio=document.createElement('audio')
  if (html5audio.canPlayType){ //check support for HTML5 audio
    for (var i=0; i<arguments.length; i++){
      var sourceel=document.createElement('source')
      sourceel.setAttribute('src', arguments[i])
      if (arguments[i].match(/\.(\w+)$/i))
        sourceel.setAttribute('type', html5_audiotypes[RegExp.$1])
      html5audio.appendChild(sourceel)
    }
    html5audio.load()
    html5audio.playclip=function(){
      html5audio.pause()
      html5audio.currentTime=0
      html5audio.play()
    }
    return html5audio
  }
  else{
    return {playclip:function(){throw new Error("Your browser doesn't support HTML5 audio unfortunately")}}
  }
}

//Initialize sound clips (should add a second clip as fallback)
var sKaboom=createsoundbite( "../img/bang.mp3");
var sSafe=createsoundbite("../img/swish.mp3");
var sWin=createsoundbite("../img/win.mp3");




});
