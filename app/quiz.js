(function(){
    angular.module('myApp.quiz6', [])
       .controller('QuizController', ['$scope', '$http', '$sce', '$routeParams', function($scope,$http,$sce, $routeParams){
        $scope.score = 0;
        $scope.numIncorrect = 0;
        $scope.startQuiz = -1;
        $scope.questionAnswered = 0;
        // var self = this;
        // self.mN = $routeParams.mN;
        $scope.mN = $routeParams.mN;
        var moduleNum = $scope.mN;//self.mN;
        var file = 'model/definitions'+moduleNum+'.json';

        $http.get(file).then(function(quizData){
            var rawQuestions = quizData.data;
            $scope.totalQuestions = rawQuestions.length;
            var allAnswers = _.pluck(rawQuestions, 'word');

            _.each(rawQuestions, function(q, index){
                q.correct = q.word;
                q.pack = _.without(allAnswers, q.correct);
                q.pack = _.sample(q.pack, 3);
                q.pack.push(q.correct);
                q.pack = _.shuffle(q.pack);
                $scope.myQuestions = rawQuestions;
            });

            $scope.selectAnswer = function(qIndex, aIndex){
                sSafe.playclip();
                var questionState = $scope.myQuestions[qIndex].questionState;
                var selectedA = $scope.myQuestions[qIndex].pack[aIndex];


                if ( questionState != 'answered' ){
                    $scope.myQuestions[qIndex].selectedAnswer = aIndex;
                    var correctAnswer = $scope.myQuestions[qIndex].correct;
                    $scope.myQuestions[qIndex].correctAnswer = correctAnswer;

                    if( $scope.myQuestions[qIndex].pack[aIndex] === correctAnswer ){
                        $scope.myQuestions[qIndex].correctness = 'correct';
                        $scope.score  += 1;
                    } else {
                        $scope.myQuestions[qIndex].correctness = 'incorrect';
                        $scope.numIncorrect  += 1;
                    }
                    $scope.myQuestions[qIndex].questionState = 'answered';

                }
                if ($scope.numIncorrect > 3) {
                    console.log("Fail!");
                    sKaboom.playclip();
                    $scope.failed = true;
                    scroll(0,0);

                }else {
                    $scope.failed = false;
                    if ($scope.score  > $scope.myQuestions.length-4) {
                        $scope.winner = true;
                    };
                }
            }

            $scope.isSelected = function(qIndex, aIndex){
                return $scope.myQuestions[qIndex].selectedAnswer === aIndex;
            }
            $scope.isCorrect = function(qIndex, aIndex){
                return $scope.myQuestions[qIndex].correct === $scope.myQuestions[qIndex].pack[aIndex];
            }
            $scope.hasFailed = function(qIndex, aIndex){
                return $scope.failed;
            }
            $scope.hasWon = function(){
                return $scope.winner;
            }
        });
            
        function shuffle(o) {
            for(var j, x, i = o.length; i; j = parseInt(Math.random() * i), x = o[--i], o[i] = o[j], o[j] = x);
            return o;
        };

        // Click sound effect
        var html5_audiotypes={ //define list of audio file extensions and their associated audio types. Add to it if your specified audio file isn't on this list:
          "mp3": "audio/mpeg"
          // ,"mp4": "audio/mp4",
          // "ogg": "audio/ogg",
          // "wav": "audio/wav"
        }

        function createsoundbite(sound){
          var html5audio=document.createElement('audio')
          if (html5audio.canPlayType){ //check support for HTML5 audio
            for (var i=0; i<arguments.length; i++){
              var sourceel=document.createElement('source')
              sourceel.setAttribute('src', arguments[i])
              if (arguments[i].match(/\.(\w+)$/i))
                sourceel.setAttribute('type', html5_audiotypes[RegExp.$1])
              html5audio.appendChild(sourceel)
            }
            html5audio.load()
            html5audio.playclip=function(){
              html5audio.pause()
              html5audio.currentTime=0
              html5audio.play()
            }
            return html5audio
          }
          else{
            return {playclip:function(){throw new Error("Your browser doesn't support HTML5 audio unfortunately")}}
          }
        }

        //Initialize sound clips (should add a second clip as fallback)
        var sKaboom=createsoundbite( "http://sprystack.com/biology/app/assets/img/bang.mp3");
        var sSafe=createsoundbite("http://sprystack.com/biology/app/assets/img/swish.mp3");
        var sWin=createsoundbite("http://sprystack.com/biology/app/assets/img/win.mp3");


     }]);



})();