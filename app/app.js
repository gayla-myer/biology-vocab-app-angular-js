(function(){

angular.module('myApp', [
  'ngRoute',
  'myApp.quiz6'
])
/**
 * Route Configuration
 */
// module
.config(["$routeProvider", "$locationProvider", Config])
// module
.controller("MainController", function ($rootScope, $route, $scope) {
    $rootScope.$on("$routeChangeSuccess", function (out) {
        console.log(out);
    });
    $scope.reloadRoute = function() {
   $route.reload();
}
})
// module
//.controller("PageController", [PageController])

.filter('startFrom', function() {
    return function(input, start) {
        if(input) {
            start = +start; //parse to int
            return input.slice(start);
        }
        return [];
    }
});

// function PageController () {
//     console.log("hey yo");
// }

/**
 * @param $routeProvider
 * @constructor
 */
function Config ($routeProvider, $locationProvider) {
    $routeProvider
        .when('/', {
            templateUrl: 'page1.html',
            controller: 'PageController'
        })
        .when('/page2', {
            templateUrl: 'page2.html',
            controller: 'PageController'
        })
        .when('/quiz/:mN', {
            templateUrl: 'quiz.html',
            controller: 'QuizController'
        });
    // $locationProvider.html5Mode({
    //     enabled: false,
    //     requireBase: true
    // });
}


})();